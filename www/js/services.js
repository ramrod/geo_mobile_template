angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { 	id: 0, 
    	name: 'About Geo21',
    	description: 'Geo21 is a game that combines the cards game of 21, and geolocation. You can basically be the major of any location, and claim it by playing 21 over it. Have fun!' },
    { 
    	id: 1,
    	name: 'How to Play',
    	description: 'The rules are simple: click on a location, play 21, if you win, you can claim that location for yourself! That is it.'}
  ];

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
});
