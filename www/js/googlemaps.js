  	//Defining map as a global variable to access from other functions
  	var map;
 
	// Styling options
	var bluishStyle = [
	    {
	        stylers: [
	            { hue: "#00FFFF" }
	        ]
	    },
	    {
	        featureType: "road",
	        elementType: "geometry",
	        stylers: [
	            { lightness: 100 },
	            { visibility: "simplified" }
	        ]
	    },
	    {
	        featureType: "water",
	        elementType: "geometry",
	        stylers: [
	            { hue: "#0000FF" },
	            {saturation:-40}
	        ]
	    },
	    {
	        featureType: "administrative.neighborhood",
	        elementType: "labels.text.stroke",
	        stylers: [
	            {
	            	color: "#E80000"
	            },
	            {
	            	weight: 1
	            	
	            }
	        ]
	     },
	    {
	        featureType: "road.highway",
	        elementType: "geometry.fill",
	        stylers: [
	            { color: "#FFFFFF" },
	            {weight: 2}
	        ]
	    }
	];
	 
 
  function initMap() {
        //Enabling new cartography and themes
    google.maps.visualRefresh = true;


 
    //Getting coordinates from device and zoom map to that location
    if (navigator.geolocation) {

    	navigator.geolocation.getCurrentPosition(function(position) {
    		var lat = position.coords.latitude;
    		var lng = position.coords.longitude;

			// Setting the styling
			var bluishStyledMap = new google.maps.StyledMapType(bluishStyle,
			    {name: "Bluish Google Base Maps with Pink Highways"});
			    
			// adding tiles from OpenStreetMap
			/*
			var osmMapType = new google.maps.ImageMapType({
			    getTileUrl: function(coord, zoom) {
			        return "http://tile.openstreetmap.org/" + zoom + 
			        "/" + coord.x + "/" + coord.y + ".png";
			    },
			    tileSize: new google.maps.Size(256, 256),
			    name: "OpenStreetMap",
			    maxZoom: 18
			});
			*/

           //Setting starting options of map
            var mapOptions = {
                  center: new google.maps.LatLng(lat, lng),
                  zoom: 15,
				  mapTypeControlOptions: {
				  	mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'new_bluish_style']
				  	// mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'OSM']
				  }
            };


            //Getting map DOM element
            var mapElement = document.getElementById('mapDiv');

            //Creating a map with DOM element which is just obtained
            map = new google.maps.Map(mapElement, mapOptions);
			map.mapTypes.set('new_bluish_style', bluishStyledMap);
			// map.mapTypes.set('OSM', osmMapType);
			map.setMapTypeId('new_bluish_style');
			// map.setMapTypeId('OSM');


			// Listener to grab the current position and open the game
	
	        google.maps.event.addListener(map, 'click', function(event) {
	          displayGame(event.latLng);
	        });
	
	        function displayGame(location) {
	              // currentMarker.setMap(null);
	              /*
	              currentMarker = new google.maps.Marker({
	                  position: location,
	                  map: map
	              });
	              */
				angular.element(document.getElementById('mainApp')).scope().openGameModal();
	        }


    	});




    } else {
    	alert('geolocation support is needed for this feature');
        }
  }

  google.maps.event.addDomListener(window, 'load', initMap);